// The libMesh Finite Element Library.
// Copyright (C) 2002-2014 Benjamin S. Kirk, John W. Peterson, Roy H. Stogner

// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



#ifndef LIBMESH_SERIAL_MESH_H
#define LIBMESH_SERIAL_MESH_H

// Local Includes -----------------------------------
#include "libmesh/serial_mesh.h"
#include "interface.h"


namespace libMesh
{






/**
 * The \p IGFEMSerialMesh class is derived from the \p SerialMesh class.
 * Most methods for this class are found in MeshBase, and most
 * implementation details are found in UnstructuredMesh.
 */

// ------------------------------------------------------------
// Mesh class definition
class IGFEMSerialMesh : public SerialMesh
{
public:
  /**
   * Constructor.  Takes \p dim, the dimension of the mesh.
   * The mesh dimension can be changed (and may automatically be
   * changed by mesh generation/loading) later.
   */
  explicit
  IGFEMSerialMesh (const Parallel::Communicator &comm_in,
              unsigned char dim=1);

#ifndef LIBMESH_DISABLE_COMMWORLD
  /**
   * Deprecated constructor.  Takes \p dim, the dimension of the mesh.
   * The mesh dimension can be changed (and may automatically be
   * changed by mesh generation/loading) later.
   */
  explicit
  IGFEMSerialMesh (unsigned char dim=1);
#endif


  /**
   * Copy-constructor.  This should be able to take a
   * serial or parallel mesh.
   */
  IGFEMSerialMesh (const UnstructuredMesh& other_mesh);

  /**
   * Copy-constructor, possibly specialized for a
   * serial mesh.
   */
  IGFEMSerialMesh (const SerialMesh& other_mesh);

  /**
   * Copy-constructor, possibly specialized for a
   * serial mesh.
   */
  IGFEMSerialMesh (const IGFEMSerialMesh& other_mesh);

  /**
   * Deep copy of another unstructured mesh class (used by subclass
   * copy constructors)
   */
  virtual void copy_nodes_and_elements(const IGFEMSerialMesh& other_mesh, const bool skip_find_neighbors=false);

  /**
   * Destructor.
   */
  virtual ~IGFEMSerialMesh();

  /**
   * Clear all internal data.
   */
  virtual void clear();

  virtual dof_id_type n_enrichment_nodes () const
  { return cast_int<dof_id_type>(_enrichment_nodes.size()); }
  virtual void reserve_enrichment_nodes (const dof_id_type nn)
  { _enrichment_nodes.reserve (nn); }

	/**
	 * Function to add an enrichment node to the mesh
	*/
	void addEnrichmentNode(Node* newNode);
	Node* get_enrichment_node (const unsigned int i) const;
	const Node& enrichment_node (const dof_id_type i) const;
	Node& enrichment_node (const dof_id_type i);

protected:
	_std::vector<Node*> _enrichment_nodes;

	/**
	 * The elements that are intersected by any interface
	*/
	std::vector<Elem*> _intersectedElem;

	/**
	 * The interfaces that part of this mesh (look more into concept of systems and subdomain)
	 * This will probably be modified
	*/
	std::vector<Interface*> _interfaces;

	void IGFEM();
	void analyze nodes();
	void analyze_elements();
	void add_enrichments();
	addElementNodeEnrichments(Node* enrichment, Elem* element);
	Point find_intersection(UniquePtr<Elem> edge);

}



inline
void IGFEMSerialMesh::addEnrichmentNode(Node* newNode)
{
	_enrichment_nodes.push_back(newNode);
}


inline
Node* IGFEMSerialMesh::get_enrichment_node (const unsigned int i) const
{
  libmesh_assert_less (i, this->n_enrichment_nodes());
  libmesh_assert(_enriched_nodes[i]);

  return _enrichment_nodes[i];
}




const Node& IGFEMSerialMesh::enrichment_node (const dof_id_type i) const
{
  libmesh_assert_less (i, this->n_enrichment_nodes());
  libmesh_assert(_enrichment_nodes[i]);
  libmesh_assert_equal_to (_enrichment_nodes[i]->id(), i); // This will change soon

  return (*_enrichment_nodes[i]);
}





Node& IGFEMSerialMesh::enrichment_node (const dof_id_type i)
{
  if (i >= this->n_enrichment_nodes())
    libmesh_error_msg(" i=" << i << ", n_enrichment_nodes()=" << this->n_enrichment_nodes());

  libmesh_assert_less (i, this->n_enrichment_nodes());
  libmesh_assert(_enrichment_nodes[i]);
  libmesh_assert_equal_to (_enrichment_nodes[i]->id(), i); // This will change soon

  return (*_enrichment_nodes[i]);
}





} //namespace libmesh
