#ifndef IGFEM_INTERFACE_PLANE
#define IGFEM_INTERFACE_PLANE

#include "interface.h"
#include <vector>

namespace libmesh
{



class Plane : public interface
{
private:
	Point _norm;	// Defines norm of plane pointing into new inclusion/interface
	Point _p				// Defines a point on the plane
public:
	/*
		Constructors
	*/
	Plane() {};
	Plane(std::vector n, point po);
	virtual int detectNode(const Node & n);
	virtual Point find_intersection(std::pair<Point, Point>& edge, double tol);

	
}


} //namespace libmesh


#endif
