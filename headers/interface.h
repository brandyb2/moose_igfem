#ifndef IGFEM_INTERFACE
#define IGFEM_INTERFACE

#include <vector>
#include "Point.h"
#include "Node.h"

namespace libmesh
{



/**
 * Maybe this inherits from DofObject? Or from something to do with a system? Subdomain?
*/

class interface
{
private:
	std::vector _domainLims;
	int _maxInterfaces;
	int _id
public:
	std::vector<float> getDomainLims() {return _domaimLims;};
	int getMaxInterfaces() {return _maxInterface;};
	void setMexInterfaces(int max) {_maxInterfaces = max;};

	int getIntNumber() {return _interfaceNumber;};
	void setIntNumber(int id) {_id = id;};
	virtual int detectNode(const Node & n)=0;
	virtual Point find_intersection(std::pair<Point, Point>& edge, double tol)=0;
}




} //namespace libmesh


#endif
